// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  server: {
    API: 'https://bioscm-811a7.firebaseio.com',
    LOGIN: 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=AIzaSyBS_Qyh53INFo77Tbvx1fI8s4GdV-iqpow'
  },
  firebaseConfig: {
    apiKey: 'AIzaSyBS_Qyh53INFo77Tbvx1fI8s4GdV-iqpow',
    authDomain: 'bioscm-811a7.firebaseapp.com',
    databaseURL: 'https://bioscm-811a7.firebaseio.com',
    projectId: 'bioscm-811a7',
    storageBucket: 'bioscm-811a7.appspot.com',
    messagingSenderId: '910948317937'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
