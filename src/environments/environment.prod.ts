export const environment = {
  production: true,
  server: {
    API: 'https://www.googleapis.com/identitytoolkit/v3/relyingparty',
    LOGIN: '/verifyPassword?key=AIzaSyBS_Qyh53INFo77Tbvx1fI8s4GdV-iqpow'
  },
  firebaseConfig : {
    apiKey: 'AIzaSyBS_Qyh53INFo77Tbvx1fI8s4GdV-iqpow',
    authDomain: 'bioscm-811a7.firebaseapp.com',
    databaseURL: 'https://bioscm-811a7.firebaseio.com',
    projectId: 'bioscm-811a7',
    storageBucket: 'bioscm-811a7.appspot.com',
    messagingSenderId: '910948317937'
  }
};
