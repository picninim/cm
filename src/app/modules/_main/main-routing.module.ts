import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main.component';

const routes: Routes = [
  {
    path: '', component: MainComponent, children: [
      { path: 'overview', loadChildren: '../overview/overview.module#OverviewModule' },
      { path: 'xrevive', loadChildren: '../xrevive/xrevive.module#XreviveModule' },
      { path: '', redirectTo: 'overview' },
    ]
  },
  { path: '**', redirectTo: 'overview' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
