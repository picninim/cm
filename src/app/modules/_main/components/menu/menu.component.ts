import { Component, OnInit, HostListener } from '@angular/core';
import { MenuCtrlService } from '../../services/menu-ctrl.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  constructor(
    private menuCtrlService: MenuCtrlService
  ) { }

  public menuGroup = [
    {
      name: 'overview',
      items: [
        {
          name: 'users',
          iconPath: 'assets/images/user.svg',
          link: ['/overview/users']
        },
        {
          name: 'devices',
          iconPath: 'assets/images/device.svg',
          link: ['/overview/devices']
        }
      ]
    },
    {
      name: 'xrevive',
      items: [
        {
          name: 'dashboard',
          iconPath: 'assets/images/dashboard.svg',
          link: ['/xrevive/dashboard']
        }
      ]
    }
  ];

  closeMenu() {
    this.menuCtrlService.close();
  }

  ngOnInit() {
  }

}
