import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MobileRibbonComponent } from './mobile-ribbon.component';

describe('MobileRibbonComponent', () => {
  let component: MobileRibbonComponent;
  let fixture: ComponentFixture<MobileRibbonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MobileRibbonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MobileRibbonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
