import { Component, OnInit, DoCheck } from '@angular/core';
import { Router, NavigationEnd, RouterEvent } from '@angular/router';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss']
})
export class BreadcrumbComponent implements OnInit, DoCheck {

  public icons = {
    users: 'assets/images/user.svg',
    devices: 'assets/images/device.svg',
    dashboard: 'assets/images/dashboard.svg'
  };

  public root: string = null;
  public url: string[] = null;
  public icon: string = null;
  public open = false;

  public toogleState(bool: boolean) {
    this.open = bool;
  }

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
    // this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe((e: RouterEvent) => this.setRoute(e.url));
  }

  ngDoCheck() {
    this.setRoute(this.router.url);
    this.setIocn();
  }

  setRoute(url: string) {
    const URL = url.split('/');
    URL.splice(0, 1);
    this.root = URL.splice(0, 1)[0];
    this.url = URL;
  }

  setIocn() {
    this.icon = this.icons[this.url[0]];
  }

  navigate(index: number) {
    console.log(index);
    const URL = this.url;
    URL.length = index + 1;
    this.router.navigate([this.root, ...URL]);
  }

}
