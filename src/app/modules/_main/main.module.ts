import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainRoutingModule } from './main-routing.module';
import { MainComponent } from './main.component';
import { SharedModule } from '../_shared/_shared.module';
import { MenuComponent } from './components/menu/menu.component';
import { BreadcrumbComponent } from './components/breadcrumb/breadcrumb.component';
import { MobileRibbonComponent } from './components/mobile-ribbon/mobile-ribbon.component';

@NgModule({
  declarations: [MainComponent, MenuComponent, BreadcrumbComponent, MobileRibbonComponent],
  imports: [
    CommonModule,
    MainRoutingModule,
    SharedModule
  ]
})
export class MainModule { }
