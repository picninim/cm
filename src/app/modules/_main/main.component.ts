import { Component, OnInit } from '@angular/core';
import { AuthService } from '../login/services/auth/auth.service';
import { HttpClient } from '@angular/common/http';
import { MenuCtrlService } from './services/menu-ctrl.service';
import { merge, combineLatest } from 'rxjs';
import { withLatestFrom } from 'rxjs/operators';
import { Animation } from '../_shared/animations';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
  animations: [ new Animation().coolFade ]
})
export class MainComponent implements OnInit {

  public isMenuVisible = true;
  public open = false;

  public toogleState(bool: boolean) {
    this.open = bool;
  }

  constructor(
    private authservice: AuthService,
    private httpClient: HttpClient,
    private menuCtrlService: MenuCtrlService
  ) { }

  shouldDisplayMenu(isSmallScreen, isVisible): boolean {
    let show = false;
    if (!isSmallScreen) {
      show = true;
      return show;
    } else {
      show = isVisible;
      return show;
    }
  }

  logout() {
    this.authservice.logout('/login');
  }

  test() {
    this.httpClient.get('https://bioscm-811a7.firebaseio.com/users.json').subscribe(e => console.log(e));
  }

  ngOnInit() {

    const smallScreenObs = this.menuCtrlService.isSmallScreen;
    const isMenuVisibleObs = this.menuCtrlService.isVisible;
    combineLatest(smallScreenObs, isMenuVisibleObs).subscribe( ([smallScreen, isMenuVisible]) => {
      this.isMenuVisible = this.shouldDisplayMenu(smallScreen, isMenuVisible);
    });
  }

}
