import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MenuCtrlService {
  public isSmallScreen = new BehaviorSubject<boolean>(false);
  public isVisible = new BehaviorSubject<boolean>(false);

  open() {
    this.isVisible.next(true);
  }

  close() {
    this.isVisible.next(false);
  }

  toogle() {
    this.isVisible.next(!this.isVisible.getValue());
  }

  constructor() {
    this.isSmallScreen.next(window.innerWidth <= 768);
    window.addEventListener('resize', (event: UIEvent) => {
      const window = event.target as Window;
      this.isSmallScreen.next(window.innerWidth <= 768);
      if (window.innerWidth > 768) {
        this.isVisible.next(false);
      }
    });
  }
}
