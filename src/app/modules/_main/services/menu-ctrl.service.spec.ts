import { TestBed } from '@angular/core/testing';

import { MenuCtrlService } from './menu-ctrl.service';

describe('MenuCtrlService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MenuCtrlService = TestBed.get(MenuCtrlService);
    expect(service).toBeTruthy();
  });
});
