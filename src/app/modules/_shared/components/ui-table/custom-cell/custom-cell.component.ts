import { Component, OnInit, Input, ViewChild, ComponentFactoryResolver, Directive, ViewContainerRef, AfterViewInit } from '@angular/core';

@Directive({
  selector: '[appCustomView]'
})
export class AppCustomViewDirective {

  constructor(public viewContainerRef: ViewContainerRef) { }

}

@Component({
  selector: 'app-custom-cell',
  templateUrl: './custom-cell.component.html',
  styleUrls: ['./custom-cell.component.scss']
})
export class CustomCellComponent implements OnInit, AfterViewInit {

  @Input() view: any;
  @Input() data: any;
  @ViewChild( AppCustomViewDirective ) appCustomViewDirective: AppCustomViewDirective;

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
  ) {

   }

  ngAfterViewInit() {
   // this.loadComponent(this.view);
  }

  ngOnInit() {
    this.loadComponent(this.view);
  }

  loadComponent( customView: any ) {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory( customView );
    const viewContainerRef = this.appCustomViewDirective.viewContainerRef;
    viewContainerRef.clear();
    const componentRef = viewContainerRef.createComponent( componentFactory );
    ( componentRef.instance as CustomViewInterface ).data = this.data;
  }

}

export interface CustomViewInterface {
  data: any;
}
