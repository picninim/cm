// tslint:disable-next-line: max-line-length
import { Component, Input, OnInit, ViewChild, ComponentFactoryResolver, Output, EventEmitter, ComponentRef, Optional, Inject } from '@angular/core';

import { InputDirective } from './input.directive';

import { TextInputComponent } from './../../../../_ui-form/components/text-input/text-input.component';
import { SelectInputComponent } from './../../../../_ui-form/components/select-input/select-input.component';
import { NG_VALIDATORS, NG_ASYNC_VALIDATORS, FormGroup, FormControl } from '@angular/forms';
import { InputBase } from 'src/app/modules/_ui-form/inputBase';
import { InputConfig } from '../../search-header/search-header.component';

@Component({
  selector: 'app-add-input',
  template: `
            <ng-template appInputHost></ng-template>
            `
})
export class DynamicInputComponent extends InputBase<any> implements OnInit {

  @Input() form: FormGroup;
  @Input() inputConfig: InputConfig;
  @ViewChild(InputDirective) appInputHost: InputDirective;

   constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
    @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>,
    ) {
    super(validators, asyncValidators);
  }
  ngOnInit() {
    this.loadComponent();
  }

  loadComponent() {

    let componentToImpress = null;

    switch (this.inputConfig.type) {
        case 'text':
          componentToImpress = TextInputComponent;
          break;
        case 'select':
          componentToImpress = SelectInputComponent;
          break;
        default:
          break;
      }

    const componentFactored = this.componentFactoryResolver.resolveComponentFactory(componentToImpress);
    const viewContainerRef = this.appInputHost.viewContainerRef;
    const inputComponent = viewContainerRef.createComponent(componentFactored) as ComponentRef<any>;

    // Pass the scope to the component
    Object.assign(inputComponent.instance, this.inputConfig.instance);

    // Set the form controll
    const controlName = this.inputConfig.formControlName || this.inputConfig.instance.placeholder;
    const abstractControl = inputComponent.instance.model.control as FormControl;
    const currentValue = this.form.get(controlName) ? this.form.get(controlName).value : null;
    abstractControl.setValue(currentValue);
    inputComponent.instance.value = currentValue;

    this.form.contains(controlName) ?
      this.form.setControl(controlName, abstractControl) : this.form.addControl(controlName, abstractControl);
  }
}
