import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appInputHost]',
})
export class InputDirective {
  constructor(public viewContainerRef: ViewContainerRef) { }
}
