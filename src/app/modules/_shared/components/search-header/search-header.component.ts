import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { SelectItemInterface } from 'src/app/modules/_ui-form/components/select-input/select-input.component';
import { InputBaseInterface } from 'src/app/modules/_ui-form/inputBase';
import { trigger, state, style, transition, animate } from '@angular/animations';

// export interface SearchInputInterface {
//   inputsConfig: InputConfig[];
// }

export interface InputConfig  {
  type: string;
  instance: InputBaseInterface;
  formControlName?: string;
  selectOptions?: SelectItemInterface[];
}

@Component({
  selector: 'app-search-header',
  templateUrl: './search-header.component.html',
  styleUrls: ['./search-header.component.scss'],
  animations: [
    trigger('flyoutLeft', [
      transition('* => void', [
        style({transform: 'translateX(0)', opacity: 1}),
        animate(300, style({transform: 'translateX(-50%)', opacity: 0}))
      ]),
      transition('void => *', [
        style({transform: 'translateX(-50%)',  opacity: 0}),
        animate(300, style({transform: 'translateX(0)',  opacity: 1}))
      ]),
    ]),
    trigger('flyoutRight', [
      transition('* => void', [
        style({transform: 'translateX(0)',  opacity: 1}),
        animate(300, style({transform: 'translateX(50%)',  opacity: 0}))
      ]),
      transition('void => *', [
        style({transform: 'translateX(50%)',  opacity: 0}),
        animate(300, style({transform: 'translateX(0)',  opacity: 1}))
      ]),
    ])
  ],
})
export class SearchHeaderComponent implements OnInit {

  @Input() title: string;
  @Input() inputs: InputConfig[];
  @Input() open = false;
  @Output() clickSearch = new EventEmitter();
  public form: FormGroup = null;

  toogleState(bool: boolean) {
    this.open = bool;
  }

  emitForm() {
    this.clickSearch.emit(this.form.value);
  }

  clearForm() {
    const controls = this.form.controls;
    for (const key in controls) {
      if (controls.hasOwnProperty(key)) {
        this.form.patchValue({
          [key]: null
        });
      }
    }
    this.clickSearch.emit(this.form.value);
  }

  constructor( private fb: FormBuilder ) { }

  ngOnInit() {
    this.form = this.fb.group({});
    console.log(this.form);
  }
}
