import {trigger, transition, style, animate, query, stagger, state} from '@angular/animations';

export class Animation {
    staggerList = trigger('listAnimation', [
        transition('* => *', [ // each time the binding value changes
            query(':leave', [
                stagger(100, [
                animate('0.3s', style({ opacity: 0 }))
                ])
            ], {optional: true}),
            query(':enter', [
                style({ opacity: 0, transform: 'translateX(-10px)' }),
                stagger(100, [
                animate('0.3s', style({ opacity: 1 , transform: 'translateX(0px)' }))
                ])
            ], {optional: true})
        ])
    ]);

    coolFade = trigger('coolFade', [
      transition(':enter', [
        animate('200ms ease-in', style({
          transform: 'scale(1)',
          'transform-origin': '0 48px',
          opacity: 1
        }))
      ]),
      transition(':leave', [
        animate('200ms ease-out', style({
          transform: 'scale(0.9)',
          'transform-origin': '0 48px',
          opacity: 0
        }))
      ]),
      state('*', style({
        transform: 'scale(1)',
        'transform-origin': '0 48px',
        opacity: 1,
        height: '*'
      })),
      state('void', style({
        transform: 'scale(0.9)',
        'transform-origin': '0 48px',
        opacity: 0,
        height: 'calc(100vh - 20vh)'
      })),
    ]);
    constructor() {

    }
}
