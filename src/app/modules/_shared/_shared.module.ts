import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS, HttpClientModule, HttpClient } from '@angular/common/http';
import { HttpInterceptorService } from '../_app/services/httpInterceptor/http-interceptor.service';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { UiTableComponent } from './components/ui-table/ui-table.component';
import { CustomCellComponent, AppCustomViewDirective } from './components/ui-table/custom-cell/custom-cell.component';
import { LoopAsObjectPipe } from './pipes/loopAsObject/loop-as-object.pipe';
import { UiFormModule } from '../_ui-form/_ui-form.module';
import { ButtonComponent } from './components/button/button.component';
import { SearchHeaderComponent } from './components/search-header/search-header.component';
import { DynamicInputComponent } from '../_shared/components/search-header/dynamic-component/input-type.component';
import { InputDirective } from '../_shared/components/search-header/dynamic-component/input.directive';

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  // tslint:disable-next-line:max-line-length
  declarations: [UiTableComponent, CustomCellComponent, LoopAsObjectPipe, AppCustomViewDirective, ButtonComponent, SearchHeaderComponent, DynamicInputComponent, InputDirective],
  imports: [
    CommonModule,
    FormsModule,
    UiFormModule,
    TranslateModule.forChild({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
    }),
    AngularSvgIconModule,
    ReactiveFormsModule,
  ],
  exports: [
    FormsModule,
    HttpClientModule,
    TranslateModule,
    AngularSvgIconModule,
    UiTableComponent,
    CustomCellComponent,
    UiFormModule,
    ReactiveFormsModule,
    ButtonComponent,
    SearchHeaderComponent,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: HttpInterceptorService, multi: true }
  ],
  entryComponents: [ DynamicInputComponent ]
})
export class SharedModule { }
