import { DashboardComponent } from './dashboard/dashboard.component';
import { XreviveComponent } from './xrevive.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '', component: XreviveComponent, children: [
      { path: 'dashboard', component: DashboardComponent },
      { path: '', redirectTo: 'dashboard' }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class XreviveRoutingModule { }
