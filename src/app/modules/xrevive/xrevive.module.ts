import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { XreviveRoutingModule } from './xrevive-routing.module';
import { XreviveComponent } from './xrevive.component';
import { DashboardComponent } from './dashboard/dashboard.component';

@NgModule({
  declarations: [XreviveComponent, DashboardComponent],
  imports: [
    CommonModule,
    XreviveRoutingModule
  ]
})
export class XreviveModule { }
