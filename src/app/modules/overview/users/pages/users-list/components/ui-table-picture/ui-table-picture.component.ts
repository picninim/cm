import { Component, OnInit, Input } from '@angular/core';
import { ninvoke } from 'q';

@Component({
  selector: 'app-ui-table-picture',
  templateUrl: './ui-table-picture.component.html',
  styleUrls: ['./ui-table-picture.component.scss']
})
export class UiTablePictureComponent implements OnInit {

  @Input() data: string = null;
  public url: string = null;

  constructor() { }

  ngOnInit() {
    const URL = this.data ? `url("${this.data}")` : 'url("assets/images/profile.svg")';
    this.url = URL;
  }
}
