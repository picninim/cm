import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UiTablePictureComponent } from './ui-table-picture.component';

describe('UiTablePictureComponent', () => {
  let component: UiTablePictureComponent;
  let fixture: ComponentFixture<UiTablePictureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UiTablePictureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UiTablePictureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
