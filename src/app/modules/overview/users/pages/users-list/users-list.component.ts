import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { UiTablePictureComponent } from './components/ui-table-picture/ui-table-picture.component';
import { HttpClient } from '@angular/common/http';
import { InfinityScrollDataService } from 'src/app/modules/_shared/services/infinity-scroll-data.service';
import { HeaderEvent } from 'src/app/modules/_shared/components/ui-table/ui-table.component';
import { BehaviorSubject, pipe } from 'rxjs';
import { skip } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { InputConfig } from 'src/app/modules/_shared/components/search-header/search-header.component';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss'],
  providers:  [ InfinityScrollDataService ]
})
export class UsersListComponent implements OnInit, OnDestroy {

  public requestParams = new BehaviorSubject<{
    orderBy: string,
    orderType: string
  }>({
    orderBy: null,
    orderType: 'descending'
  });

  public tableHeaders = ['', 'email', 'username', 'id', 'u_sysdt', 'i_sysdt', 'u_prgid', 'i_prid', 'u_user', 'i_userid'];
  public tableData = [];
  public customViews = {
    picture: UiTablePictureComponent
  };
  public noDataTitle: string = null;
  public loadingData = true;

  public inputs: InputConfig[] = [
    {
      type: 'text',
      instance: {
        placeholder: 'email'
      }
    },

    {
      type: 'select',
      instance: {
        placeholder: 'select',
        selectOptions: [
          {
            value: 'asdas',
            label: 'asdasdasd'
          }
        ]
      }
    }

  ];

  constructor(
    private infinityScrollDataService: InfinityScrollDataService,
    private httpClient: HttpClient,
    private translateServices: TranslateService
  ) {
    this.infinityScrollDataService.config(
      {
        httpClient: this.httpClient,
        path: 'https://httpbin.org/get',
        scrollElement: document.getElementsByClassName('main-container')[0] as HTMLElement,
        params: this.requestParams.getValue(),
      }
    );
    this.infinityScrollDataService.beforeGetData( () => {
      this.loadingData = true;
    });
    this.infinityScrollDataService.onGetData((res) => {
      this.loadingData = false;
      this.tableData.push({
        picture: null,
        email: 'email@email.com',
        username: 'picninim',
        id: 1,
        u_sysdt: new Date().toDateString(),
        i_sysdt: new Date().toDateString(),
        u_prid: 1,
        i_prid: 1,
        u_userid: new Date().toDateString(),
        i_userid: new Date().toDateString(),
      },
      {
        picture: null,
        email: 'email@email.com',
        username: 'picninim',
        id: 1,
        u_sysdt: new Date().toDateString(),
        i_sysdt: new Date().toDateString(),
        u_prid: 1,
        i_prid: 1,
        u_userid: new Date().toDateString(),
        i_userid: new Date().toDateString(),
      },
      {
        picture: null,
        email: 'email@email.com',
        username: 'picninim',
        id: 1,
        u_sysdt: new Date().toDateString(),
        i_sysdt: new Date().toDateString(),
        u_prid: 1,
        i_prid: 1,
        u_userid: new Date().toDateString(),
        i_userid: new Date().toDateString(),
      },
      {
        picture: null,
        email: 'email@email.com',
        username: 'picninim',
        id: 1,
        u_sysdt: new Date().toDateString(),
        i_sysdt: new Date().toDateString(),
        u_prid: 1,
        i_prid: 1,
        u_userid: new Date().toDateString(),
        i_userid: new Date().toDateString(),
      },
      {
        picture: null,
        email: 'email@email.com',
        username: 'picninim',
        id: 1,
        u_sysdt: new Date().toDateString(),
        i_sysdt: new Date().toDateString(),
        u_prid: 1,
        i_prid: 1,
        u_userid: new Date().toDateString(),
        i_userid: new Date().toDateString(),
      },
      {
        picture: null,
        email: 'email@email.com',
        username: 'picninim',
        id: 1,
        u_sysdt: new Date().toDateString(),
        i_sysdt: new Date().toDateString(),
        u_prid: 1,
        i_prid: 1,
        u_userid: new Date().toDateString(),
        i_userid: new Date().toDateString(),
      },    {
        picture: null,
        email: 'email@email.com',
        username: 'picninim',
        id: 1,
        u_sysdt: new Date().toDateString(),
        i_sysdt: new Date().toDateString(),
        u_prid: 1,
        i_prid: 1,
        u_userid: new Date().toDateString(),
        i_userid: new Date().toDateString(),
      },    {
        picture: null,
        email: 'email@email.com',
        username: 'picninim',
        id: 1,
        u_sysdt: new Date().toDateString(),
        i_sysdt: new Date().toDateString(),
        u_prid: 1,
        i_prid: 1,
        u_userid: new Date().toDateString(),
        i_userid: new Date().toDateString(),
      },
      {
        picture: null,
        email: 'email@email.com',
        username: 'picninim',
        id: 1,
        u_sysdt: new Date().toDateString(),
        i_sysdt: new Date().toDateString(),
        u_prid: 1,
        i_prid: 1,
        u_userid: new Date().toDateString(),
        i_userid: new Date().toDateString(),
      },
      {
        picture: null,
        email: 'email@email.com',
        username: 'picninim',
        id: 1,
        u_sysdt: new Date().toDateString(),
        i_sysdt: new Date().toDateString(),
        u_prid: 1,
        i_prid: 1,
        u_userid: new Date().toDateString(),
        i_userid: new Date().toDateString(),
      },
      {
        picture: null,
        email: 'email@email.com',
        username: 'picninim',
        id: 1,
        u_sysdt: new Date().toDateString(),
        i_sysdt: new Date().toDateString(),
        u_prid: 1,
        i_prid: 1,
        u_userid: new Date().toDateString(),
        i_userid: new Date().toDateString(),
      },
      {
        picture: null,
        email: 'email@email.com',
        username: 'picninim',
        id: 1,
        u_sysdt: new Date().toDateString(),
        i_sysdt: new Date().toDateString(),
        u_prid: 1,
        i_prid: 1,
        u_userid: new Date().toDateString(),
        i_userid: new Date().toDateString(),
      });
      console.log(res);
    });
    this.infinityScrollDataService.start();
  }

  clickheader($event: HeaderEvent) {
    const params = this.requestParams.getValue();
    params.orderBy = $event.header;
    params.orderType = $event.order;
    this.requestParams.next(params);
  }

  receiveForm(clickSearch) {
    console.log(clickSearch);
  }

  ngOnInit() {
    this.requestParams.pipe(skip(1)).subscribe( e => {
      this.tableData = [];
      this.infinityScrollDataService.clear().start();
    });
    this.translateServices.get('NO_USERS_FOUND').subscribe( t =>  this.noDataTitle = t);

    console.log('init pai');
    // setInterval( () => {

    //   console.log(this.test);
    // }, 1000);

    // setInterval(() => {
    //   this.loadingData = !this.loadingData;
    // }, 1000);
  }

  ngOnDestroy() {
    this.infinityScrollDataService.destroy();
  }

}
