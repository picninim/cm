import { Component, OnInit } from '@angular/core';
import { InfinityScrollDataService } from '../../_shared/services/infinity-scroll-data.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-devices',
  templateUrl: './devices.component.html',
  styleUrls: ['./devices.component.scss'],
  providers: [InfinityScrollDataService]
})
export class DevicesComponent implements OnInit {

  constructor(
    private infinityScrollDataService: InfinityScrollDataService,
    private httpClient: HttpClient
  ) {
    this.infinityScrollDataService.config({
      httpClient: this.httpClient,
      path: 'https://httpbin.org/get',
      scrollElement: document.getElementsByClassName('main-container')[0] as HTMLElement
    });
    this.infinityScrollDataService.onGetData((res) => {
      // this.tableData = this.tableData.concat(this.tableData);
      console.log(res);
    });

    this.infinityScrollDataService.beforeGetData( () => {
      this.infinityScrollDataService.setPath('https://httpbin.org/get?');
    });
  }

  ngOnInit() {
    this.infinityScrollDataService.destroy();
  }

}
