import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DevicesRoutingModule } from './devices-routing.module';
import { DevicesComponent } from './devices.component';
import { DeviceViewComponent } from './pages/device-view/device-view.component';
import { DevicesListComponent } from './pages/devices-list/devices-list.component';

@NgModule({
  declarations: [DevicesComponent, DeviceViewComponent, DevicesListComponent],
  imports: [
    CommonModule,
    DevicesRoutingModule,
  ]
})
export class DevicesModule { }
