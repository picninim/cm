import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { tap } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private angularFireAuth: AngularFireAuth,
    private httpClient: HttpClient,
    private router: Router
  ) { }

  getUser(): User {
   return JSON.parse(localStorage.getItem('user'));
  }

  setUser(user: User): void {
    localStorage.setItem('user', JSON.stringify(user));
  }

  login(email: string, password: string): Observable<any> {
    return this.httpClient.post(environment.server.LOGIN, {email, password, returnSecureToken: true}).pipe(tap((user: User) => {
        // tslint:disable-next-line:no-string-literal
        user.token = user.token || user['refreshToken'];
        this.setUser(user);
        console.log('setou user');
      }));
  }

  logout(redirectTo?: string) {
    localStorage.removeItem('user');
    if (redirectTo) {
      this.router.navigate([redirectTo]);
    }
  }
}

export interface User {
  id: string;
  name: string;
  token: string;
}
