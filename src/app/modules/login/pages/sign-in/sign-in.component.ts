import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth/auth.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {

  public user = {
    email: 'user@cm.com',
    password: 'cm2019'
  };

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  async login() {
    return await this.authService.login(this.user.email, this.user.password).subscribe(() => {
      this.router.navigate(['']);
    });
  }

  ngOnInit() {
  }

}
