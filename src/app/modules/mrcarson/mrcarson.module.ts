import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MrcarsonRoutingModule } from './mrcarson-routing.module';
import { MrcarsonComponent } from './mrcarson.component';

@NgModule({
  declarations: [MrcarsonComponent],
  imports: [
    CommonModule,
    MrcarsonRoutingModule
  ]
})
export class MrcarsonModule { }
