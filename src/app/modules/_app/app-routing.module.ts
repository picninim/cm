import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginGuard } from '../login/guards/login/login.guard';
import { LogoutGuard } from '../login/guards/logout/logout.guard';

export const routes: Routes = [
  { path: 'login', loadChildren: '../login/login.module#LoginModule', canActivate: [LogoutGuard] },
  { path: '', loadChildren: '../_main/main.module#MainModule', canActivate: [LoginGuard] },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
