import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

const LANGS_GROUPS = {
  en: ['en', 'en-au', 'en-bz', 'en-ca', 'en-ie', 'en-jm', 'en-ph', 'en-tt', 'en-gn', 'en-us', 'en-zw'],
  zn: ['zn', 'zn-cn', 'zn-sg', 'zn-tw']
};

@Injectable({
  providedIn: 'root'
})
export class TranslateConfigService {

  private browserLang: string = null;
  private defaultTranlateLang: string = null;

  constructor(
    private translateSerivce: TranslateService
  ) {
    this.browserLang = navigator.language || 'en';
    this.defaultTranlateLang = this.extractLang(this.browserLang);
  }

  private extractLang(lang: string): string {
    let foundLang: string = null;
    for (const key in LANGS_GROUPS) {
      if (LANGS_GROUPS.hasOwnProperty(key)) {
        const list = LANGS_GROUPS[key] as Array<string>;
        foundLang = list.includes(lang.toLocaleLowerCase()) ? key : null;
        if (foundLang) {
          break;
        }
      }
    }

    return foundLang || 'en';
  }

  init() {
    this.translateSerivce.setDefaultLang(this.defaultTranlateLang);
    this.translateSerivce.use(this.defaultTranlateLang);
  }

}
