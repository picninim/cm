import { UiFormStylesControllerDirective } from './ui-form-styles-controller.directive';
import { ElementRef, Renderer2 } from '@angular/core';
const REN: Renderer2 = null;

describe('UiFormStylesControllerDirective', () => {
  it('should create an instance', () => {
    const directive = new UiFormStylesControllerDirective( new ElementRef({}), REN);
    expect(directive).toBeTruthy();
  });
});
