import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TextInputComponent } from './components/text-input/text-input.component';
import { FormsModule } from '@angular/forms';
import { UiFormStylesControllerDirective } from './directives/ui-form-styles-controller.directive';
import { SelectInputComponent } from './components/select-input/select-input.component';
import { AngularSvgIconModule } from 'angular-svg-icon';

@NgModule({
  declarations: [TextInputComponent, UiFormStylesControllerDirective, SelectInputComponent],
  imports: [
    CommonModule,
    FormsModule,
    AngularSvgIconModule
  ],
  exports: [TextInputComponent, SelectInputComponent],
  entryComponents: [ SelectInputComponent, TextInputComponent]
})
export class UiFormModule {

}
