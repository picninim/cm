// tslint:disable-next-line: max-line-length
import { Component, OnInit, forwardRef, AfterViewChecked, AfterViewInit, Optional, Inject, HostListener, Input, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { NG_VALUE_ACCESSOR, NG_VALIDATORS, NG_ASYNC_VALIDATORS } from '@angular/forms';
import { InputBase } from '../../inputBase';

import * as Pikaday from 'pikaday';
import * as moment from 'moment';

@Component({
  selector: 'app-date-input',
  templateUrl: './date-input.component.html',
  styleUrls: ['./date-input.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => DateInputComponent),
    multi: true
  }]
})
export class DateInputComponent extends InputBase<Date> implements OnInit, AfterViewChecked, AfterViewInit {

  @Input() format = 'MM/dd/yyyy';
  @Input() mask = '99/99/9999';
  @Output() dateParsed = new EventEmitter<Date>();

  @ViewChild('uiModel') inputEl: ElementRef;

  private picker: Pikaday;
  public viewValue: Date;
  // private initValue = false;

  constructor(
    @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
    @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>,
  ) {
    super(validators, asyncValidators);
    // console.log(this.value);
  }

  parseValue(value: Date): string {

    const date = moment(value).format(this.format);
    console.log(date);
    return date.replace(/\//g, '');
  }

  ngModelChange($event: string) {
    console.log($event);
    const valid = this.isValid($event);
    let date = null;
    if (valid) {
      date = moment($event, this.format).toDate();
    }
    this.value = date;
    // this.value = moment($event, this.format).toDate();
    this.dateParsed.emit(this.value);
  }

  ngAfterViewInit() {
    // this.field = document.getElementById(this.inputEl);

    this.viewValue = this.value;
    console.log(this.value);

    this.picker = new Pikaday({
      field: this.inputEl.nativeElement,
      onSelect: (date: Date) => {
          this.value = date;
      },
      i18n: {
        previousMonth : 'Anterior',
        nextMonth     : 'Próximo',
        months        : [
          'Janeiro', 'Fevereiro', 'Março', 'Abril',
          'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro',
          'Outubro', 'Novembro', 'Dezembro'
        ],
        weekdays      : [
          'Domingo', 'Segunda-feira', 'Terça-feira',
          'Quarta-feira', 'Quinta-feira', 'Sexta-feira', 'Sábado'
        ],
        weekdaysShort : [
          'Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'
        ]
      }
    });
  }

  isValid(dateString: string) {
    let valid = true;
    const date = moment(dateString, this.format).toDate();
    if (dateString.length < this.mask.length) {
      valid = false;
    } else {
// tslint:disable-next-line: no-shadowed-variable
      const date = moment(dateString, this.format).toDate() as any;
      // tslint:disable-next-line: triple-equals
      if ( date == 'Invalid Date') {
        valid = false;
      }
    }

    if (!valid) {
      this.model.control.setErrors({
        invalidFormat: true
      });
    }
    return valid;
  }

  ngAfterViewChecked() {
    // console.log('checl');
    // if (this.value !== undefined && !this.initValue) {
    //   this.picker.setDate(
    //     moment(this.value, this.format).toDate());
    //   this.initValue = true;
    // }
  }

  @HostListener('keyup', ['$event'])
  onKeyup($event: any) {
    // let valor = $event.target.value.replace(/\D/g, '');
    // const pad = this.mask.replace(/\D/g, '').replace(/9/g, '_');
    // const valorMask = valor + pad.substring(0, pad.length - valor.length);

    // console.log(this.value);
    // // retorna caso pressionado backspace
    // if ($event.keyCode === 8) {
    //   // this.changeValue(valor);
    //   // this.onChange(valor);
    //   return;
    // }

    // let valorMaskPos = 0;
    // valor = '';
    // for (let i = 0; i < this.mask.length; i++) {
    //   if (isNaN(parseInt(this.mask.charAt(i), 10))) {
    //     valor += this.mask.charAt(i);
    //   } else {
    //     valor += valorMask[valorMaskPos++];
    //   }
    // }

    // if (valor.indexOf('_') > -1) {
    //   valor = valor.substr(0, valor.indexOf('_'));
    // }

    // $event.target.value = valor;
    // this.onChange(valor);
  }

  ngOnInit() {
    console.log(this.value);
  }

}
